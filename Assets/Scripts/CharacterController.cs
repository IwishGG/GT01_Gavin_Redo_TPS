﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterController : Unit
{

    [Header("Player")]
    [SerializeField]
    private float _moveSpeed = 8f;

    [SerializeField]
    private float _sprintSpeedMult = 5f;

    [SerializeField]
    private float _jumpSpeed = 12f;

    [SerializeField]
    private Transform _camPivot;


    private float _xInput;
    private float _yInput;
    private float _mouseX;
    private float _mouseY;
    private float _mouseScroll;
    private float _speedMult;
    private bool _jumpPressed;
    private Camera _camera;
    private Rigidbody _rigidBody;

    override
    protected void UnitAwake()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _camera = _camPivot.GetComponentInChildren<Camera>();
    }

    override
    protected void UnitStart()
    {
    }


    private void Update()
    {
        ReadInputs();
        ApplyCameraZoom();

        if (IsAlive == false) return;

        UpdateAnimations();
        Shooting();
        ApplyRotations();
    }

    private void FixedUpdate()
    {
        if (IsAlive == false) return;

        ApplyPhysics();
    }


    private void ReadInputs()
    {
        _xInput = Input.GetAxis("Horizontal");
        _yInput = Input.GetAxis("Vertical");
        _mouseX = Input.GetAxis("Mouse X");
        _mouseY = Input.GetAxis("Mouse Y");
        _mouseScroll = Input.mouseScrollDelta.y;
        _jumpPressed |= Input.GetButtonDown("Jump");
        _speedMult = Input.GetKey(KeyCode.LeftShift) ? _sprintSpeedMult : 1f;
    }

    private void UpdateAnimations()
    {
        _anim.SetFloat("MoveX", _xInput);
        _anim.SetFloat("MoveZ", _yInput);
        _anim.SetFloat("Sprint", _speedMult);
    }

    private void ApplyRotations()
    {
        transform.Rotate(0f, _mouseX, 0f);
        _camPivot.Rotate(-_mouseY, 0f, 0f);
    }

    private void ApplyCameraZoom()
    {
        var camPos = _camera.transform.localPosition;
        camPos.z += _mouseScroll;
        camPos.z = Mathf.Clamp(camPos.z, -32f, 0f); // Hardcoded
        _camera.transform.localPosition = camPos;
    }

    private void ApplyPhysics()
    {
        // Movement Physics
        var moveVector = new Vector3(_xInput, 0f, _yInput) * _moveSpeed * _speedMult;
        moveVector = transform.TransformVector(moveVector); // Move according to local orientation

        // Jump
        if (_jumpPressed) _anim.SetTrigger("Jump");
        moveVector.y = _jumpPressed ? _jumpSpeed : _rigidBody.velocity.y;
        _jumpPressed = false; // Consumed Jump Button Press

        _rigidBody.velocity = moveVector;
    }

    private void Shooting()
    {
        // press LMB on this frame
        if (Input.GetMouseButtonDown(0) == false) return;

        if (Physics.Raycast(_camera.transform.position, _camera.transform.forward, out var hit))
        {
            if (CanSee(hit.point, hit.transform))
            {
                var otherUnit = hit.transform.GetComponent<Unit>();
                if (otherUnit != null && otherUnit.TeamNumber != TeamNumber) // Avoid Friendly fire
                {
                    ShootLasersFromEyes(hit.point, otherUnit);
                }
            }
        }
    }


}
