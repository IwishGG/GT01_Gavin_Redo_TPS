using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent))]
public class AIController : Unit
{

    [SerializeField]
    private float _enemyDetectionRadius = 8f;

    [SerializeField]
    private float _timeBetweenAttacks = 1f;

    [SerializeField]
    private LayerMask _unitLayerMask;

    [SerializeField]
    private Image _healthImage;
    private Canvas _healthCanvas;


    private NavMeshAgent _navAgent = null;
    private Coroutine _currentState = null;
    private Outpost _currentOutpost = null;
    private Unit _currentTarget = null;
    private float _attackTimer = 0f;

    override
    protected void UnitAwake()
    {
        _navAgent = GetComponent<NavMeshAgent>();
        _healthCanvas = _healthImage.GetComponentInParent<Canvas>();
    }

    override
    protected void UnitStart()
    {
        SetState(State_Idle());
    }

    private void Update()
    {
        if (IsAlive == false) return;

        _anim.SetFloat("MoveZ", _navAgent.velocity.magnitude);
    }


    private Outpost GetValidOutpost()
    {
        Outpost v = Outpost.GetRandom();
        if (v == null) return null;
        if (v.CaptureValue < 1f || v.CurrentTeam != TeamNumber)
        {
            return v;
        }
        return null;
    }

    private void SetEnemyUnitAroundMe()
    {
        if (IsAlive == false) return;
        if (_currentTarget != null && _currentTarget.IsAlive) return;
        var aroundMe = Physics.OverlapSphere(transform.position, _enemyDetectionRadius, _unitLayerMask);
        foreach (var item in aroundMe)
        {
            if (item.transform == transform) continue; // Ignores Self

            var otherUnit = item.GetComponent<Unit>();
            if (otherUnit != null && otherUnit.IsAlive && otherUnit.TeamNumber != TeamNumber)
            {
                _currentTarget = otherUnit;
                SetState(State_AttackingEnemy());
                return;
            }
        }
    }

    private void SetState(IEnumerator newState)
    {
        if (IsAlive == false) newState = State_Dead();

        if (_currentState != null)
        {
            StopCoroutine(_currentState);
        }
        _currentState = StartCoroutine(newState);
    }

    //This is my default state, i will always come back here to go where i need to be
    private IEnumerator State_Idle()
    {
        while (_currentOutpost == null)
        {
            _currentOutpost = GetValidOutpost();//Outpost.GetRandom();
            yield return null;
        }

        SetState(State_MovingToOutpost());
    }

    private IEnumerator State_MovingToOutpost()
    {
        _navAgent.SetDestination(_currentOutpost.transform.position);
        _navAgent.isStopped = false;

        while (_navAgent.isActiveAndEnabled && _navAgent.remainingDistance > _navAgent.stoppingDistance)
        {
            SetEnemyUnitAroundMe();
            yield return null;
        }

        SetState(State_CapturingOutpost());
    }

    private IEnumerator State_CapturingOutpost()
    {
        while (_currentOutpost != null && _currentOutpost.CaptureValue < 1f)
        {
            SetEnemyUnitAroundMe();
            yield return null;
        }

        _currentOutpost = null;
        SetState(State_Idle());
    }

    private IEnumerator State_AttackingEnemy()
    {
        _navAgent.isStopped = true;

        while (_currentTarget != null && _currentTarget.IsAlive)
        {
            _attackTimer += Time.deltaTime;
            var lookPos = _currentTarget.transform.position.With(y: transform.position.y);
            transform.LookAt(lookPos);
            if (_attackTimer >= _timeBetweenAttacks)
            {
                ShootLasersFromEyes(_currentTarget.transform.position + Vector3.up, _currentTarget);
                _attackTimer -= _timeBetweenAttacks;
            }
            yield return null;
        }

        _currentTarget = null;
        SetState(State_Idle());
    }

    private IEnumerator State_Dead()
    {
        _currentTarget = null;
        _currentOutpost = null;
        if (_navAgent.isActiveAndEnabled)
        {
            _navAgent.isStopped = true;
            _navAgent?.ResetPath();
            _navAgent.enabled = false;
        }
        var col = GetComponent<Collider>();
        if (col != null) Destroy(col);
        yield return null;
    }

    override
    public void OnHit(float dmg)
    {
        if (IsAlive == false) return;

        base.OnHit(dmg);
        _healthImage.fillAmount = _health / 100f;
    }

    override
    protected void OnDie()
    {
        base.OnDie();
        SetState(State_Dead());
        _healthCanvas.gameObject.SetActive(false);
    }

}
