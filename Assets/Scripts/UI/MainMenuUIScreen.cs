﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuUIScreen : UIScreen
{

    protected void OnEnable()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 1f;
    }

    public void ButtonPlay()
    {
        SceneManager.LoadScene("GameplayScene");
        UIScreenController.ShowScreen<GameplayUIScreen>();
    }
}
