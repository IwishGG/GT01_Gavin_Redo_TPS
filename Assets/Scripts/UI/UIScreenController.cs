﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UIScreenController : MonoBehaviour
{
    public static Dictionary<Type, UIScreen> TypeUIScreenMap { get; private set; } = new Dictionary<Type, UIScreen>();

    private static UIScreen _currentScreen;

    protected void Awake()
    {
        PopulateDictionary();
        DontDestroyOnLoad(gameObject);
    }

    protected void OnEnable()
    {
        ShowScreen<MainMenuUIScreen>();
    }

    private void PopulateDictionary()
    {
        foreach (var screen in GetComponentsInChildren<UIScreen>(true))
        {
            screen.gameObject.SetActive(false);
            var type = screen.GetType();
            TypeUIScreenMap.Add(type, screen);
            Debug.Log($"Registered UISCreen => {type}", screen);
        }
    }

    public static void ShowScreen<T>() where T : UIScreen
    {
        if (_currentScreen != null)
        {
            _currentScreen.gameObject.SetActive(false);
        }
        var type = typeof(T);
        _currentScreen = TypeUIScreenMap[type];

        _currentScreen.gameObject.SetActive(true);
    }

}
