﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverUIScreen : UIScreen
{

    [SerializeField]
    private Image _bgImage;


    [SerializeField]
    private TMP_Text _text;

    protected void OnEnable()
    {
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.None;
        int winner = GameManager.Instance.Winner;
        _bgImage.color = GameManager.Instance.TeamColors[winner];
        _text.text = $"Game Over\n Team {winner} Won!";
    }

    public void ButtonRestart()
    {
        UIScreenController.ShowScreen<GameplayUIScreen>();
        SceneManager.LoadScene("GameplayScene");
    }

    public void ButtonQuit()
    {
        UIScreenController.ShowScreen<MainMenuUIScreen>();
        SceneManager.LoadScene("MainMenuScene");
    }
}
