﻿using System.Collections;
using UnityEngine;

public class CouroutineIntro : MonoBehaviour
{

    [SerializeField]
    private int _framesToWait = 5;

    private void OnEnable()
    {
        print(0);
        StartCoroutine(MyFirstCoroutine());
        print(999);
    }

    private IEnumerator MyFirstCoroutine()
    {
        while (true)
        {
            print(Time.time);
            yield return new WaitForSeconds(3f);
            yield return null; // waits 1 frame
        }
    }

}
