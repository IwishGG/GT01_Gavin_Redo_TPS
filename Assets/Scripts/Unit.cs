using UnityEngine;

public abstract class Unit : MonoBehaviour
{

    [Header("Unit")]
    public int TeamNumber = 0;

    [SerializeField]
    protected float _health = 100f;

    [SerializeField]
    protected float _damage = 8f;

    [SerializeField]
    protected Renderer _renderer;

    [SerializeField]
    protected Laser _laserPrefab;

    protected Animator _anim;
    protected Eye[] _eyes;

    public bool IsAlive => _health > 0f;

    abstract
    protected void UnitAwake();
    protected void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        UnitAwake();

        _eyes = GetComponentsInChildren<Eye>();

        Vector3 v3 = transform.position.With(y: 0f, x: 99f);
    }


    abstract
    protected void UnitStart();
    protected void Start()
    {
        SetTeam(TeamNumber);
        UnitStart();
    }

    public void SetTeam(int team)
    {
        TeamNumber = team;
        if (_renderer != null)
        {
            _renderer.material.color = GameManager.Instance.TeamColors[TeamNumber];
        }
    }

    /// <summary>
    /// Raycast from each eye to the pos, to see if any eye can see the other transform
    /// </summary>
    protected bool CanSee(Vector3 pos, Transform other)
    {
        foreach (var eye in _eyes)
        {
            var eyePos = eye.transform.position;
            var dir = pos - eyePos;
            if (Physics.Raycast(eyePos, dir, out var hit) && hit.transform == other)
            {
                return true;
            }
        }
        return false;
    }

    protected void ShootLasersFromEyes(Vector3 pos, Unit target)
    {
        foreach (var eye in _eyes)
        {
            Instantiate(_laserPrefab).Shoot(eye.transform.position, pos);
        }

        target?.OnHit(_damage);
    }


    virtual
    public void OnHit(float dmg)
    {
        if (IsAlive == false) return;
        _health -= dmg;
        if (_health <= 0f)
        {
            _health = 0f;
            OnDie();
        }
    }

    virtual
    protected void OnDie()
    {
        _anim.SetBool("IsAlive", false);
    }

}
