﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AITester : MonoBehaviour
{

    private NavMeshAgent _navAgent;
    private Camera _cam;

    private void Awake()
    {
        _navAgent = GetComponent<NavMeshAgent>();
        _cam = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            var ray = _cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit, 1000f))
            {
                _navAgent.SetDestination(hit.point);
            }
        }
    }
}
