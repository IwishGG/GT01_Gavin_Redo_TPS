﻿using UnityEngine;

public class LooAtCamera : MonoBehaviour
{
    private Transform _cam;

    protected void Awake()
    {
        _cam = Camera.main.transform;
    }

    protected void LateUpdate()
    {
        transform.LookAt(_cam);
    }
}
