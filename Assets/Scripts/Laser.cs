﻿using UnityEngine;

public class Laser : MonoBehaviour
{
    [SerializeField]
    private float _lifeTime = 0.05f;

    public void Shoot(Vector3 from, Vector3 to)
    {
        var line = GetComponent<LineRenderer>();
        line.SetPosition(0, from);
        line.SetPosition(1, to);
        Destroy(gameObject, _lifeTime);
    }
}
